---
header-includes:
- \usepackage[margins=raggedright]{floatrow}
--- 



# Codes

## Q-Codes

| Code   | Als vraag                                | Als antwoord |
| :--    | :--                                      | :-- |
| QRK    | Wat is mijn neembaarheid?                | Uw neembaarheid is ... |
| QRM    | Wordt u gestoord?                        | Ik word gestoord |
| QRN    | Hebt u luchtstoring                      | Ik heb luchtstoring |
| QRO    | Moet ik meer zendvermogen gebruiken      | Verhoog zendvermogen |
| QRP    | Moet ik minder zendvermogen gebruiken    | Verlaag zendvermogen |
| QRT    | Moet ik ophouden                         | Houd op |
| QRV    | Bent u beschikbaar                       | Ik ben beschikbaar |
| QRX    | Op welk tijdstip roept u mij             | Ik roep weer om ... uur |
| QRZ    | Wie roept mij                            | ... roept u |
| QSB    | Verandert sterkte van mijn signaal       | Uw signaalsterkte veranderd |
| QSL    | Wilt u mij de ontvangst bevestigen       | Ik bevestig u de ontvangst |
| QSO    | Kunt u rechtstreeks met ... werken       | Ik kan rechtstreeks met ... werken |
| QSY    | Zal ik op andere frequentie zenden       | Ga op een andere frequentie zenden |
| QTH    | Wat is uw positie                        | Mijn positie is ... |

## Overige codes


| Code   | Betekenis |
| :--    | :--       |
| BK     | BREAK - een lopende uitzending onderbreken |
| CQ     | SEEK YOU - Algemene oproep aan alle stations |
| CW     | Continuous Wave - ononderbroken draaggolf |
| DE     | "van", gebruikt om de roepletters van opgeroepen en roepende station te scheiden |
| K      | Uitnodiging om te zenden, "over" |
| MSG    | Bericht |
| PSE    | Please |
| RST    | Readability, Signal Strength, Tone quality |
| DX     | Long-distance |
| R      | Ontvangen |
| TX     | Ontvanger |
| TX     | Zender |
| UR     | Your, uw |





