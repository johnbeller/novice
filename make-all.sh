#!/bin/bash
for f in *.md; do
	pandoc $f -o ${f%.*}.pdf
done
pdflatex veron-formules.tex
rm *.log  *.aux

