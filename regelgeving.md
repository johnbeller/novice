---
header-includes:
- \usepackage[margins=raggedright]{floatrow}
--- 



# Regelgeving

## Afkortingen


| Afkorting      | Betekenis  |
| :--            | :-     |
| ITU            | International Telecommunication Union |
| CEPT           | Conference Europeaenne des Administrations des Postes et des Telecommunications (...) |
| IARU           | International Amateur Radio Union (binnen de ITU) |
| HAREC          | Harmonized Amateur Radio Examination Certificate |
| RR             | Radio Reglement (ITU) |



## NL Call Signs (Roepnamen)

- Serie voor Nederland
  - PAA..PIZ
- Voor amateurs: twee letters + cijfer + 1..3 letters (*suffix*)
  - PA..PI
    - PA..PH voor individuele zendamateurs
    - PI voor verenigingen, instellingen en experimenten
    - PD voor Novice
  - Cijfer: 
    - 0..5 en 7..9 voor individuele zendamateurs
    - 6 voor evenementen, conteststations etc.
  - Suffix:
    - 1..3 letters zelf kiezen
    - SOS mag niet
    - QOA..QUZ mogen ook niet
- Buitenlandse bezoekers:
  - PA/ gevolgd door hun eigen roepnaam
  - Of, eigen roepnaam gevolgd door /PA of /PD (Novice)

## ITU Regio's
  
| Regionummer    | Regio  |
| :--            | :-     |
| I              | Europa, Afrika, Rusland, Mongolie, Iraq en nog wat landen |
| II             | Noord- en Zuid-Amerika, Groenland |
| III            | Rest van de wereld |





