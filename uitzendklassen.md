---
header-includes:
- \usepackage[margins=raggedright]{floatrow}
--- 



# Uitzendklassen


COE = Class of Emissions = KVU = Kenmerk van uitzending

| Eerste teken      | Tweede teken       | Derde teken          |
| :--               | :--                | :-                   |
| {N,A,H,J,R,G,C,F} | {0,1,2,3}          | {N,A,B,C,D,E,F}      |
| = *Modulatie*     | = *Draaggolf*      | = *Soort informatie* |


## 1: Modulatie

| Letter | Modulatie                                |
| :--    | :--                                      |
| N      | Ongemoduleerde draaggolf, meetdoeleinden |
| **A**  | **AM (dubbelzijband), incl. CW**         |
| H      | SSB maar met *volledige* draaggolf       |
| **J**  | **Normale amateur SSB**                  |
| R      | SSB met restdraaggolf/pilot tone         |
| G      | Fasemodulatie PM data/spraak             |
| C      | Analog amateur TV (vervangen door DATV)  |
| **F**  | **FM**                                   |

## 2: Draaggolf

| Cijfer | Draaggolf                                |
| :--    | :--                                      |
| 0      | Geen modulatie                           |
| 1      | Niet-analoog, **zonder** modulerende draaggolf |
| 2      | Niet-analoog, **met** modulerende draaggolf |
| 3      | analoge info, spraak, fax etc            |

## 3: Soort informatie

| Letter | Soort informatie                         |
| :--    | :-                                       |
| N      | Geen informatie, pieptoon                |
| A      | Hoorbare morse                           |
| B      | telex, rtty                              |
| C      | stilstaand beeld (fax, sstv)             |
| D      | data                                     |
| E      | telefonie (spraak)                       |
| F      | televisie (bewegend beeld)               |


## Deze kennen voor N-examen

~~~~
A1A         Hoorbare morse

A3E         AM, analoog, spraak

F1A         FM, niet-analoog, morse
F1B         FM, niet-analoog, telegrafie
F1D         FM, niet-analoog, data

F2C         FM, niet-analoog, beeld

F3E         FM, analoog, spraak

G3E         Fasemodulatie, analoog, spraak

J2A         SSB, niet-analoog, morse
J3E         SSB, analoog, spraak

~~~~

## Overzicht 


~~~~
spraak              A3E 
                    H3E 
                    J3E 
                    R3E 
                    F3E 
                    G3E                  

morse       A1A A2A 
            F1A F2A 
                J2A 
            G1A G2A              

rtty        A1B A2B 
            F1B F2B 
                J2B                      

data        F1D F2D                                  

fax         A1C A2C A3C 
                J2C J3C 
            F1C F2C F3C 

sstv        G1C G2C G3C 

ATV                 A3F 
                    C3F 
                    F3F 
~~~~




