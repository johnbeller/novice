---
header-includes:
- \usepackage[margins=raggedright]{floatrow}
--- 



# Weerstanden

| Kleur	      | Band I | Band II | Band III, x   | 
| :--         | :--    | :--     | :--           |
| Zwart       |   0    |   0     |   1e0 = 1     |
| Bruin       |   1    |   1     |   1e1 = 10    |
| Rood        |   2    |   2     |   1e2 = 100   |
| Oranje      |   3    |   3     |   1e3 = 1000  |
| Geel        |   4    |   4     |   1e4 ...     |
| Groen       |   5    |   5     |   1e5         |
| Blauw       |   6    |   6     |   1e6         |
| Violet      |   7    |   7     |   1e7         |
| Grijs       |   8    |   8     |   1e8         |
| Wit         |   9    |   9     |   1e9         |

Ezelsbruggetje: **Z**ij **BR**acht **RO**zen **O**p **GE**rrits **GR**af **B**ij **VI**es **GR**ijs **W**eer

## Toleranties

| Kleur   | Tolerantie   |
| :--     | :--          |
| Niks    | 20%          |
| Zilver  | 10%          |
| Goud    | 5%           |




